import { environment } from './../../environments/environment';

export class AppEndpoints {
  public static readonly CASE_DETAILS_API: string = environment.apiUrl + '/api/mdm/transaction/caseDetails';
  public static readonly INVOICE_DETAILS_API: string = environment.apiUrl + '/api/mdm/transaction/invoiceDetails';
  public static readonly BANK_DETAILS_API: string = environment.apiUrl + '/api/mdm/transaction/bankDetails';
  public static readonly INITIATE_BID_API: string = environment.apiUrl + '/api/mdm/transaction/initiateBid';
  public static readonly SUBMIT_FINAL_DETAILS_API='http://192.168.60.140:3001/api/scf';
}
