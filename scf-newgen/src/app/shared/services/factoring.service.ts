import { AppEndpoints } from './../app-endpoints';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class FactoringService {

  constructor(private httpClient: HttpClient) { }
  postCaseDetails(caseDetailsObj): Observable<any> {


    return this.httpClient.post(AppEndpoints.CASE_DETAILS_API, caseDetailsObj);
    // .pipe(map((res) => {
    //   return { isDetailsSaved: true, res };

    // }
    // ))

  }

  postInvoiceDetails(invoiceDetailsObj, factorBean): Observable<any> {

    return this.httpClient.post(AppEndpoints.INVOICE_DETAILS_API, invoiceDetailsObj).pipe(
      mergeMap(res => this.httpClient.post(AppEndpoints.CASE_DETAILS_API, factorBean))
    );
  }

  postBankDetails(bankDetailsObj, factorBean): Observable<any> {

    return this.httpClient.post(AppEndpoints.BANK_DETAILS_API, bankDetailsObj).pipe(
      mergeMap((res) => this.httpClient.post(AppEndpoints.INITIATE_BID_API, factorBean))

    )
  }
  postFinalDetails(finalBean): Observable<any> {

    return this.httpClient.post(AppEndpoints.SUBMIT_FINAL_DETAILS_API,finalBean);
  }

}
