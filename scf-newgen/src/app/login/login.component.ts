import { UserAuthenticationService } from './../shared/services/user-authentication.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private fb: FormBuilder,
    private userAuthenticationService: UserAuthenticationService,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {

    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  performLogin() {
    const result = this.userAuthenticationService.login(this.loginForm.controls.username.value, this.loginForm.controls.password.value)

    if (result == true) {
      this.toastr.success('Success', 'Login Successfully!');
      this.router.navigate(['/home']);
    }
    else {
      this.toastr.error('Failed','Invalid Credentials!');
    }
  }
  get f() {
    return this.loginForm.controls;
  }
}
