import { LandingPageComponent } from './landing-page/landing-page.component';
import { TransactionComponent } from './transaction/transaction.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        component: LandingPageComponent
    },
      {
        path: 'transaction',
        component: TransactionComponent
      }

      // {
      //     path: 'listPreference',
      //     component: ListPreferenceComponent
      // },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
