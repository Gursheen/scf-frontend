import { HttpClient } from '@angular/common/http';
import { FactoringService } from './../../shared/services/factoring.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import Stepper from 'bs-stepper';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  tabs = ['HOME', 'TRANSACTION', 'MY ACTIONABLE', 'SCENARIOS', 'HISTORY', 'REPORTS', 'ADVANCE FEES', 'REQUEST',
    'PREFERENCES'];
  productType = ['Assignment of Receivables', 'Sales Invoice Discounting', 'Factoring Bill of Exchange', 'Dealer Financing']

  selectedTab = 'TRANSACTION';

  factorBean = {};
  invoiceBean = {};
  bankBean = {};
  finalData = {};
  stepper: Stepper;
  doneCaseDetails = false;
  doneInvoiceDetails = false;
  doneBankDetails = false;
  factorDetails: any;
  caseDetailsForm: FormGroup;
  invoiceDetailsForm: FormGroup;
  bankDetailsForm: FormGroup;

  constructor(private fb: FormBuilder,
    private factoringService: FactoringService,
    private toastr: ToastrService) { }

  ngOnInit() {

    this.caseDetailsForm = this.fb.group({
      caseUnitID: [{ value: '', disabled: true }],
      productType: ['', Validators.required],
      buyerName: ['', Validators.required],
      invDate: ['', [Validators.required]],
      dueDate: [''],
      bidValidityDate: [''],
      maxBidRate: ['', Validators.required],
      autoAcceptRate: ['', Validators.required],
      uniqueReferenceID: [{ value: '', disabled: true }],
      totalInvAmt: [{ value: '', disabled: true }],
      totalAmtClaimed: [{ value: '', disabled: true }],
      maxAcceptableMargin: ['', Validators.required],
      commidityType: ['', Validators.required],
      commidity: ['', Validators.required],
      remarks: ['', Validators.required],
      buyerCustId: ['BUYER00000034'],
      factorId: [''],
      factorName: [''],
      factorAcceptDate: [''],
      chargesBorneBy: ['ChargesBorneBy'],
      decision: ['Accepted'],
      compliance: [''],
      sellerIntrest: [''],
      buyerIntrest: [''],
      bOENo: [''],
      productCode: ['FF'],
      status: ['SaveAsDraft'],
      docIndex: [''],
      boeType: [''],
      companyName: ['Newgen'],
      resettleStatus: ['']
    });

    this.invoiceDetailsForm = this.fb.group({
      invNo: ['', Validators.required],
      invAmt: ['', Validators.required],
      amtClaimed: ['', Validators.required],
      poNo: ['', Validators.required],
      intRefNo: ['', Validators.required],
      grnDatenew: ['', Validators.required],
      grnNonew: ['', Validators.required],
      factorId: [],
      hiddenId: [''],
      invoiceAction: ['A'],
      remarks: ['']
    })

    this.bankDetailsForm = this.fb.group({
      factorId: [''],
      role: ['Seller'],
      UserName: ['Gursheen'],
      CustId: ['SELLER00000028'],
      str: ['Bank B|8910291|SDG|SDF|Supplier Bank|true|23.00|true|Y|Credit'],
      bankName: [{ value: "Bank A", disabled: true }],
      accountNumber: [{ value: '8910291', disabled: true }],
      micr: [{ value: "SDG", disabled: true }],
      ifsc: [{ value: "SDF", disabled: true }],
      bankType: [{ value: "Supplier Bank", disabled: true }],
      mandateLetter: [{ value: "true", disabled: true }],
      coAcceptLimit: [{ value: "23.00", disabled: true }],
      coAcceptMandLetter: [{ value: "Y", disabled: true }],
      accountingTransaction: [{ value: 'Credit', disabled: true }],
      remarks: ['']

    })

    this.stepper = new Stepper(document.querySelector('#stepper'), {
      linear: true,
      animation: true
    })
  }

  onTabSelected(tab: string) {
    this.selectedTab = tab;
  }

  next(tab: string) {
    if (tab == 'caseDetails') {
      this.doneCaseDetails = true;
    }
    else if (tab == 'invoiceDetails') {
      this.doneInvoiceDetails = true;
    }
    else {
      this.doneBankDetails = true;
    }
    this.stepper.next();
  }

  onSubmit() {
    window.alert('done');
  }
  formatResponse(result: any) {
    let output = JSON.stringify(result)
    output = JSON.parse(output)
    output = output["#result-set-1"][0]["response"];
    let formattedResponse: any;
    if (output.indexOf("~") > -1) {
      formattedResponse = output.split("~");
    }
    else {
      formattedResponse = output;
    }
    return formattedResponse;
  }
  submitCaseDetails(caseDetailsObj) {
    this.factorBean = caseDetailsObj;
    this.factoringService.postCaseDetails(caseDetailsObj).subscribe((response) => {
      this.factorDetails = this.formatResponse(response);

      this.factorBean['factorId'] = this.factorDetails[0];
      this.factorBean['factorName'] = this.factorDetails[2];

      // window.alert(this.factorBean['factorId'] + ' created successfully!')
      this.toastr.success('Success', this.factorBean['factorId'] + ' created successfully!')

    }, (error) => {
      let errorMessage;

      switch (error.status) {
        case 401:
          errorMessage = 'Invalid Details!';
          break;

        case 404:
          errorMessage = 'No such user found!';
          break;

        case 500:
          errorMessage = 'Internal Server Error!';
          break;

        default:
          errorMessage = 'Failed to Create Factor ID!';
          break;
      }

      // window.alert('Failure: ' + errorMsg);
      this.toastr.error('Failed', errorMessage);
    });
    console.log("case details form submitted", caseDetailsObj)
  }

  submitInvoiceDetails(invoiceDetailsObj) {
    this.invoiceBean = invoiceDetailsObj;
    this.invoiceBean['factorId'] = this.factorBean['factorId'];
    this.factoringService.postInvoiceDetails(this.invoiceBean, this.factorBean).subscribe((response) => {

      this.factorDetails = this.formatResponse(response);
      // window.alert(this.factorDetails[1]);
      this.toastr.success('Success', this.factorDetails[1] + '!');
    }, (error) => {
      let errorMessage = error.message;

      switch (error.status) {
        case 401:
          errorMessage = 'Invalid Details!';
          break;

        case 404:
          errorMessage = 'Resource Not found!';
          break;

        case 500:
          errorMessage = 'Internal Server Error!';
          break;

        default:
          errorMessage = 'Failed to Save Invoice Details!';
          break;
      }
      // window.alert('Failure' + errorMessage);
      this.toastr.error('Failed', errorMessage);
    });

  }

  submitBankDetails(bankDetailsObj) {
    this.bankBean = bankDetailsObj;
    this.bankBean['factorId'] = this.factorBean['factorId'];

    this.factoringService.postBankDetails(this.bankBean, this.factorBean).subscribe((response) => {
      this.doneBankDetails = true;
      this.factorDetails = this.formatResponse(response);
      // window.alert(this.factorDetails);
      this.toastr.success('Success', this.factorDetails + '!');
      this.submitFinalData(this.factorBean, this.invoiceBean, this.bankBean);
    }, (error) => {
      let errorMessage;
      switch (error.status) {
        case 401:
          errorMessage = 'Invalid Details!';
          break;

        case 404:
          errorMessage = 'No such user found!';
          break;

        default:
          errorMessage = 'Failed to Initiate Factoring Request!';
          break;
      }
      // window.alert('Failure: ' + errorMsg);
      this.toastr.error('Failed', errorMessage);
    });

    console.log("bank details form submitted")
  }
  submitFinalData(factorbean, invoicebean, bankbean) {
    let finalData;

    finalData = Object.assign(factorbean, invoicebean, bankbean);

    this.factoringService.postFinalDetails(finalData).subscribe((response) => {
      console.log(response);
      this.toastr.success('Success', 'Transaction Hash Genereated -: ' + response.data);

    }, (error) => {
      let errorMessage;
      switch (error.status) {
        case 401:
          errorMessage = 'Invalid Details!';
          break;

        case 404:
          errorMessage = 'No such user found!';
          break;
        case 500:
          errorMessage = 'Internal Server Error!';
          break;

        default:
          errorMessage = 'Failed to send data to hyperledger!';
          break;
      }
      // window.alert('Failure: ' + errorMsg);
      this.toastr.error('Failed', errorMessage);
    }
    )
  };

  resetForm(formName: FormGroup) {
    formName.reset();
  }

  get caseDetails() {
    return this.caseDetailsForm.controls;
  }
  get invoiceDetails() {
    return this.invoiceDetailsForm.controls;
  }
  get bankDetails() {
    return this.bankDetailsForm.controls;
  }
}
